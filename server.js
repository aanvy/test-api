const express = require("express")
const bodyParser = require("body-parser")
const secureRandom = require("secure-random")
const compression = require("compression")
const jwt = require("jsonwebtoken")
const { person } = require("./person")
const app = express()
const signingKey = secureRandom(256, { type: "Buffer" })

const BASEURL = "/api"

app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
)

app.use(bodyParser.json())

const groups = []

const ID = function() {
  return (
    "_" +
    Math.random()
      .toString(36)
      .substr(2, 9)
  )
}

app.post(`${BASEURL}/authenticate`, (req, res) => {
  const pwd = "test"
  const { username, password } = req.body
  const user = person.find(user => user.username === username)
  if (user && password === pwd) {
    jwt.sign(
      { user: user.username },
      signingKey,
      { expiresIn: "1d" },
      (err, token) => {
        if (err) {
          res.send({
            err,
          })
        }
        res.send({ token })
      },
    )
  } else {
    throw new Error("could not login")
  }
})

const validateToken = (req, res, next) => {
  const token = req.headers["authorization"]
  if (typeof token != "undefined") {
    jwt.verify(token.trim(), signingKey, (err, res, data) => {
      if (err) {
        res.sendStatus(401)
      } else {
        next()
      }
    })
  } else {
    res.sendStatus(401)
  }
}

app.get(`${BASEURL}/groups`, validateToken, (req, res) => {
  res.send(groups)
})

app.post(`${BASEURL}/groups`, validateToken, (req, res) => {
  const token = req.headers["authorization"]
  const tokenDetails = jwt.decode(token)
  const group = {
    ...req.body,
    groupId: ID(),
    persons: [],
    owner: tokenDetails.user,
  }

  groups.push(group)
  res.send(group)
})

app.delete(`${BASEURL}/groups/:groupId`, validateToken, (req, res) => {
  res.sendStatus(201)
})

app.get(`${BASEURL}/person/:searchTerm`, validateToken, (req, res) => {
  const search = req.params.searchTerm
  const results = person.filter(
    data =>
      data.name.toLowerCase().match(search.toLowerCase()) ||
      data.username.toLowerCase().match(search.toLowerCase()) ||
      data.email.toLowerCase().match(search.toLowerCase()),
  )
  res.send(results)
})

app.put(
  `${BASEURL}/groups/:groupId/person/:personId`,
  validateToken,
  (req, res) => {
    const { groupId, personId } = req.params
    const personObj = person.find(
      personObj => personObj.id.toString() === personId.toString(),
    )
    if (personObj) {
      const group = groups.find(data => data.groupId === groupId)
      group.persons = [
        ...group.persons,
        person.find(
          personObj => personObj.id.toString() === personId.toString(),
        ),
      ]
      groups.map(item => (item.groupId === groupId ? group : item))
      res.send(group)
    } else {
      throw new Error("person not found")
    }
  },
)

app.delete(
  `${BASEURL}/groups/:groupId/person/:personId`,
  validateToken,
  (req, res) => {
    const { groupId, personId } = req.params
    const group = groups.find(data => data.groupId === groupId)
    group.persons = group.persons.filter(data => data.id != personId)
    groups.map(item => (item.groupId === groupId ? group : item))
    res.send(group)
  },
)

app.use((err, req, res, next) => {
  res.sendStatus(err.status || 400).json({ message: err.message })
})

app.listen(5015, () => {
  console.log("app listenning on port 5015")
})
